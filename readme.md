# Backend server example for Node.js

__04/08/2020:__

This example comes from the [Udemy.com](https://www.udemy.com/) course _The Complete Node.js Developer Course (3rd Edition)_ with Andrew Mead. It's an excellent course that I am recommending!

This is the end result for then Task-manager, from section 10 to section 16.

It uses express to server the API and MongoDB for the database. It provides a whole implementation for Authentication/Authorization so the users can only access their own data.

It shows you how to deal with sorting, pagination and filtering (kind of a OData implementation). It also supports file upload (via multer) and emails (via SendGrid).

## The environment variables

There are 2 files related to the environment variable in the folde ```./config```: dev.env and test.env

The only difference between the two is the mongoDB connection:

__dev.env__
```
PORT=3000
SENDGRID_API_KEY=xxxxxxxxxxx
MONGODB_URL=mongodb://127.0.0.1:27017/task-manager-api
JWT_SECRET=thisisasecretformyapp
```

__test.env__
```
PORT=3000
SENDGRID_API_KEY=xxxxxxxxxxx
MONGODB_URL=mongodb://127.0.0.1:27017/task-manager-api-test
JWT_SECRET=thisisasecretformyapp
```



